const startBtn = document.getElementById('btn-start')
const stopBtn = document.getElementById('btn-stop')
const resetBtn = document.getElementById('btn-reset')

const minField = document.getElementById('minutes')
const secField = document.getElementById('seconds')
const milliSecondsField = document.getElementById('milliseconds')

const watch = document.querySelector('.watch')

let min = 00
let sec = 00
let milliSec = 00

let interval

startBtn.addEventListener('click', ()=>{

    interval = setInterval(startTimer, 10)
    startBtn.setAttribute('disabled', true)  
    watch.classList.add('watch--active')
})
stopBtn.addEventListener('click', ()=>{
    clearInterval(interval)
    startBtn.removeAttribute('disabled') 
    
})
resetBtn.addEventListener('click', ()=>{
    clearInterval(interval)

    minField.innerText = '00:' 
    secField.innerText = '00:' 
    milliSecondsField.innerText = '00'
    milliSec = 0
    sec = 0
    min = 0
    startBtn.removeAttribute('disabled')
    watch.classList.remove('watch--active') 
})

function startTimer(){
    
    milliSec++
    
    milliSec > 9 ? milliSecondsField.innerText = `${milliSec}` : milliSecondsField.innerText =`0${milliSec}`  
    if(milliSec > 99){
        milliSec = 0
        milliSecondsField.innerText =`0${milliSec}`
        sec++
        sec > 9 ? secField.innerText = `${sec}:` : secField.innerText =`0${sec}:`      
    }
    if(sec > 59){
        min++
        min > 9 ? minField.innerText = `${min}:` : minField.innerText = `0${min}:`
        sec = 0
        secField.innerText = secField.innerText =`0${sec}:`
    }
}
